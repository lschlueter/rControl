package de.lennox.rcontrol.console.command;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by Lennox on 18.04.18.
 */
public class CommandRegistry {

    private static final Map<String, Command> commandMap = new HashMap<>();

    public static void registerCommand(Command command) {
        commandMap.put(command.getName().toLowerCase(), command);
    }

    public static boolean dispatchCommand(String commandLine) {
        Pattern argsSplit = Pattern.compile(" ");
        String[] split = argsSplit.split(commandLine);

        // Check for chat that only contains " "
        if (split.length == 0) {
            return false;
        }

        String commandName = split[0].toLowerCase();
        Command command = commandMap.get(commandName);

        // Check for alias
        if (command == null) {
            return false;
        }

        String[] args = Arrays.copyOfRange(split, 1, split.length);
        try {
            command.execute(args);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

}
