package de.lennox.rcontrol.console.command.commands;

import de.lennox.rcontrol.console.command.Command;
import de.lennox.rcontrol.service.DiscoThread;
import de.lennox.rcontrol.service.GpioService;
import lombok.extern.log4j.Log4j2;

/**
 * Created by Lennox on 19.04.18.
 */
@Log4j2
public class CommandLED extends Command {

    public CommandLED() {
        super("led");
    }

    @Override
    public void execute(String[] args) {
        if (args.length != 1) {
            log.info("---------------------------------------------------");
            log.info("led <on> | starts the led");
            log.info("led <off> | starts the led");
            log.info("led <disco> | makes a 10s disco effect");
            log.info("---------------------------------------------------");
            return;
        }

        switch (args[0]) {
            case "on":
                log.info("Sending toggle request..");
                new GpioService().toggleLed(GpioService.GpioState.ON);
                break;
            case "off":
                log.info("Sending toggle request..");
                new GpioService().toggleLed(GpioService.GpioState.OFF);
                break;
            case "disco":
                log.info("Starting disco..");
                new DiscoThread().start();
                break;
        }
    }
}
