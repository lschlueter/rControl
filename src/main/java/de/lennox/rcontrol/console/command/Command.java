package de.lennox.rcontrol.console.command;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by Lennox on 18.04.18.
 */
@AllArgsConstructor
public abstract class Command {

    @Getter
    private String name;

    public abstract void execute(String[] args);
}