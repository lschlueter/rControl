package de.lennox.rcontrol.console;

import de.lennox.rcontrol.console.command.CommandRegistry;
import lombok.extern.log4j.Log4j2;

import java.util.Scanner;

/**
 * Created by Lennox on 18.04.18.
 */
@Log4j2
public class ConsoleThread extends Thread {

    private Scanner scanner;

    public ConsoleThread() {
        this.scanner = new Scanner(System.in);
    }

    @Override
    public void run() {
        String line;
        while ((line = scanner.nextLine()) != null) {
            this.checkLine(line);
        }
    }

    public void checkLine(String line) {
        if (!CommandRegistry.dispatchCommand(line)) {
            log.error("Command not found!");
        }
    }
}
