package de.lennox.rcontrol;

import de.lennox.rcontrol.console.ConsoleThread;
import de.lennox.rcontrol.console.command.CommandRegistry;
import de.lennox.rcontrol.console.command.commands.CommandLED;
import de.lennox.rcontrol.service.GpioService;
import lombok.extern.log4j.Log4j2;

/**
 * Created by Lennox on 17.04.18.
 */
@Log4j2
public class RControl {

    public static void main(String[] args) throws InterruptedException {
        // start console
        ConsoleThread consoleThread = new ConsoleThread();
        consoleThread.start();

        // register commands
        CommandRegistry.registerCommand(new CommandLED());

        // prepare
        new GpioService().prepare();

        log.info("RControl started! Type help to see all commands.");
    }
}
