package de.lennox.rcontrol.service;

import java.util.concurrent.TimeUnit;

/**
 * Created by Lennox on 22.04.18.
 */
public class DiscoThread extends Thread {

    private GpioService.GpioState state = GpioService.GpioState.OFF;

    @Override
    public void run() {
        int i = 0;
        GpioService service = new GpioService();
        while (i < 100) {
            if (this.state == GpioService.GpioState.OFF) {
                this.state = GpioService.GpioState.ON;
            } else this.state = GpioService.GpioState.OFF;
            service.toggleLed(state);
            try {
                TimeUnit.MILLISECONDS.sleep(80);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
    }
}
