package de.lennox.rcontrol.service;

import lombok.extern.log4j.Log4j2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Lennox on 19.04.18.
 */
@Log4j2
public class GpioService {

    public void prepare() {
        List<String> command = Arrays.asList("echo", "14", ">", "/sys/class/gpio/export");
        ProcessBuilder processBuilder = new ProcessBuilder(command);
        try {
            processBuilder.start();
        } catch (IOException e) {
            log.error("Could not prepare GPIO14! " + e.getMessage());
            return;
        }
        command = Arrays.asList("echo", "out", ">", "/sys/class/gpio/gpio14/direction");
        processBuilder = new ProcessBuilder(command);
        try {
            processBuilder.start();
        } catch (IOException e) {
            log.error("Could not set output of GPIO14! " + e.getMessage());
            return;
        }

        log.info("Successfully prepared GPIO-14!");
    }

    public void toggleLed(GpioState state) {
        if (state == GpioState.OFF) {
            this.writeFile("/sys/class/gpio/gpio14/value", "0");
        } else {
            this.writeFile("/sys/class/gpio/gpio14/value", "1");
        }
        log.info("STATE: " + state);
    }

    private void writeFile(String path, String s) {
        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter(path);
            printWriter.write(s);
            printWriter.flush();
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public enum GpioState {

        ON, OFF
    }
}
